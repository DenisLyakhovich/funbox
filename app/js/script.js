/*--------------------------- col-packages-1 conditions -------------------------------*/

// defaulthover 
$( ".col-packages-1" ).on({
  mouseenter: function() {
    $( ".circle", this ).addClass( "defaulthover" ); 
  }, 
  mouseleave: function() {
    $( ".circle", this ).removeClass( "defaulthover" ); 
  },
});

// selected  
$( ".col-packages-1" ).click( function( event ) {
  $( this ).toggleClass( "selected" );
  $( ".circle", this ).toggleClass( "selected" );
  $( ".col-descriptions-1 .descriptions-text-2" ).toggle();
  $( ".col-descriptions-1 .descriptions-text-1" ).toggle("invisibletext");
});

// selectedhover 
$( ".col-packages-1" ).click(function( event ) {
  $( ".col-packages-1" ).on( "mouseleave", function( event ) {
    $( ".circle", this ).toggleClass( "selectedhover" );
    $( ".text-conteiner .text-change-1", this ).toggle();
    $( ".text-conteiner .text-1", this ).toggle("invisibletext");
  });
  $( ".col-packages-1" ).on( "mouseenter", function( event ) {
    $( ".circle", this ).toggleClass( "selectedhover" );
    $( ".text-conteiner .text-change-1", this ).toggle();
    $( ".text-conteiner .text-1", this ).toggle("invisibletext");
  });  

});

/*--Delet this comment, add comment // selected cection // selectedhover cection and you can use //disable cection --
// disable 
$( ".col-packages-1" ).click( function( event ) {
  $( this ).addClass( "disableshadow" );
  $( ".circle", this ).addClass( "disable" );
  $( ".col-descriptions-1 .descriptions-text-3" ).addClass("visibletext");
  $( ".col-descriptions-1 .descriptions-text-1" ).addClass("invisibletext"); 
});
-------------------------------------------------------------------------------------------------------------------*/

/*--------------------------- col-packages-2 conditions -------------------------------*/

// defaulthover  
$( ".col-packages-2").on({
  mouseenter: function() {
    $( ".circle", this ).addClass( "defaulthover" ); 
  }, 
  mouseleave: function() {
    $( ".circle", this ).removeClass( "defaulthover" ); 
  },
});

// selected  
$( ".col-packages-2" ).click( function( event ) {
  $( this ).toggleClass( "selected" );
  $( ".circle", this ).toggleClass( "selected" );
  $( ".col-descriptions-2 .descriptions-text-2" ).toggle();
  $( ".col-descriptions-2 .descriptions-text-1" ).toggle("invisibletext");
});

// selectedhover 
$( ".col-packages-2" ).click(function( event ) {
  $( ".col-packages-2" ).on( "mouseleave", function( event ) {
    $( ".circle", this ).toggleClass( "selectedhover" );
    $( ".text-conteiner .text-change-1", this ).toggle();
    $( ".text-conteiner .text-1", this ).toggle("invisibletext");
  });
  $( ".col-packages-2" ).on( "mouseenter", function( event ) {
    $( ".circle", this ).toggleClass( "selectedhover" );
    $( ".text-conteiner .text-change-1", this ).toggle();
    $( ".text-conteiner .text-1", this ).toggle("invisibletext");
  });  
});

/*--Delet this comment, add comment // selected cection // selectedhover cection and you can use //disable cection --
// disable 
$( ".col-packages-2" ).click( function( event ) {
  $( this ).addClass( "disableshadow" );
  $( ".circle", this ).addClass( "disable" );
  $( ".col-descriptions-2 .descriptions-text-3" ).addClass("visibletext");
  $( ".col-descriptions-2 .descriptions-text-1" ).addClass("invisibletext"); 
});
-------------------------------------------------------------------------------------------------------------------*/


/*--------------------------- col-packages-3 conditions -------------------------------*/

// defaulthover  
$( ".col-packages-3" ).on({
  mouseenter: function() {
    $( ".circle", this ).addClass( "defaulthover" ); 
  }, 
  mouseleave: function() {
    $( ".circle", this ).removeClass( "defaulthover" ); 
  },
});

/*-- Delet this comment, add comment // disable section and you can use //selected section //selectedhover section --
// selected  
$( ".col-packages-3" ).click( function( event ) {
  $( this ).toggleClass( "selected" );
  $( ".circle", this ).toggleClass( "selected" );
  $( ".col-descriptions-3 .descriptions-text-2" ).toggle();
  $( ".col-descriptions-3 .descriptions-text-1" ).toggle("invisibletext");
});

// selectedhover 
$( ".col-packages-3" ).click(function( event ) {
  $( ".col-packages-3" ).on( "mouseleave", function( event ) {
    $( ".circle", this ).toggleClass( "selectedhover" );
    $( ".text-conteiner .text-change-1", this ).toggle();
    $( ".text-conteiner .text-1", this ).toggle("invisibletext");
  });
  $( ".col-packages-3" ).on( "mouseenter", function( event ) {
    $( ".circle", this ).toggleClass( "selectedhover" );
    $( ".text-conteiner .text-change-1", this ).toggle();
    $( ".text-conteiner .text-1", this ).toggle("invisibletext");
  });  
});
------------------------------------------------------------------------------------------------------------------*/

// disable 
$( ".col-packages-3" ).click( function( event ) {
  $( this ).addClass( "disableshadow" );
  $( ".circle", this ).addClass( "disable" );
  $( ".col-descriptions-3 .descriptions-text-3" ).addClass("visibletext");
  $( ".col-descriptions-3 .descriptions-text-1" ).addClass("invisibletext"); 
});








